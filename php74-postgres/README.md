# Instalación Wordpress con PostgreSQL

## 1. Configuración de la red

```bash
$ docker network create --subnet 172.18.0.0/16 --gateway 172.18.0.1 net-olce
```

## 2. Instalación y Configuración de PostgreSQL

```bash
# Descargar la imagen desde docker hub
$ docker pull postgres:12.9-alpine

# Creación de un volumen persistente para almacenamiento de las bases de datos
$ docker volume create postgres-data
$ docker volume inspect postgres-data
[
    {
        "CreatedAt": "2023-01-14T18:17:54-05:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/postgres-data/_data",
        "Name": "postgres-data",
        "Options": {},
        "Scope": "local"
    }
]

# Iniciar el contenedor con la imagen descargada
$ docker run -d --ip 172.18.0.3 --add-host postgres-host:172.18.0.3 --net net-olce --name postgres-server -p 5432:5432 -v postgres-data:/var/lib/postgresql/data -e "POSTGRES_PASSWORD=olce2023" postgres:12.9-alpine

# Interactuamos con el contenedor creado
$ docker exec -it postgres-server sh
```

## 3. Instalación y Configuración de Wordpress

### 3.1 Creación de la base de datos PostgreSQL

A continuación se crea la base de datos necesaria para Wordpress

```
# Desde el contendor iniciado nos conectamos a la base de datos postgres
psql -U postgres -d postgres

postgres=# create user wpadmin with encrypted password 'wp2023';
postgres=# create database wp_db;
postgres=# grant all privileges on database wp_db to wpadmin;
```

### 3.2 Creación de la imagen docker

Editar el archivo src/wp-config.php con los valores de conexión a la base de datos creada en el paso anterior

```php
// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_db' );

/** Database username */
define( 'DB_USER', 'wpadmin' );

/** Database password */
define( 'DB_PASSWORD', 'wp2023' );

/** Database hostname */
define( 'DB_HOST', '172.18.0.3' );
```

Crear la imagen docker desde el archivo Dockerfile
```
docker build -t wp-alpine .
```

### 3.3 Inicialización de Wordpress

Inicializamos el contenedor con la imagen creada en el paso anterior

```bash
# Iniciar el contenedor con la imagen descargada
$ docker run -d --ip 172.18.0.5 --net net-olce --name wordpress -p 80:80 wp-alpine
```

### 3.4 Instalación de Wordpress

Accedemos al URL del sitio wordpress http://olce.innovate.pe

Ingresamos siguiente información:
- Site Title: OLCE
- Username: admin
- Password: ****
- Your Email: ****

A continuación hacemos click en "Install WordPress"

![instalación de wordpress](img/wp-1.png)

Ignoramos los errores (se deben a que por ser la primera instalación, las tablas en postgres necesitan ser creadas), y damos click en "Log In"

![instalación de wordpress](img/wp-2.png)

Ingresmos el usuario y clave para acceder al consola de administración

![instalación de wordpress](img/wp-3.png)

A continuación se muestra la consola de administración

![instalación de wordpress](img/wp-4.png)



docker run -d --ip 172.18.0.5 --net net-olce --name wordpress -p 80:80 wp-alpine

docker exec -it postgres-server sh